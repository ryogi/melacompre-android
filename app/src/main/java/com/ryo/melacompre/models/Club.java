package com.ryo.melacompre.models;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by ryogi on 29/12/16.
 */

public class Club {

    private String name;

    private String email;

    private String address;

    private ArrayList<Event> events;

    public Club(String name) {
        this.name = name;
        events = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public ArrayList<Event> getEvents() {
        return events;
    }

    public void addEvent(Event e) {
        events.add(e);
    }

    public void setEvents(ArrayList<Event> events) {
        this.events = events;
    }
}
