package com.ryo.melacompre.models;

import android.graphics.Color;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

/**
 * Created by ryogi on 29/12/16.
 */

public class Event {

    private String name;

    private Club club;

    private Date date;

    private ArrayList<Locality> localities;

    public Event(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Club getClub() {
        return club;
    }

    public void setClub(Club club) {
        this.club = club;
    }

    public Date getDate() {
        return date;
    }

    public String getFormatDate(String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(date);
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public ArrayList<Locality> getLocalities() {
        return localities;
    }

    public void setLocalities(ArrayList<Locality> localities) {
        this.localities = localities;
    }
}
