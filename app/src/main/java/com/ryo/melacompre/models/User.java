package com.ryo.melacompre.models;

import java.util.ArrayList;

/**
 * Created by ryogi on 29/12/16.
 */

public class User {

    private String email;

    private String name;

    private String iDNumeber;

    private int points;

    private ArrayList<Event> events;

    public User(String email, String name, String iDNumeber, int points) {
        this.email = email;
        this.name = name;
        this.iDNumeber = iDNumeber;
        this.points = points;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getiDNumeber() {
        return iDNumeber;
    }

    public void setiDNumeber(String iDNumeber) {
        this.iDNumeber = iDNumeber;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public ArrayList<Event> getEvents() {
        return events;
    }

    public void setEvents(ArrayList<Event> events) {
        this.events = events;
    }
}
