package com.ryo.melacompre.models;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ryogi on 29/12/16.
 */

public class Locality {

    private String name;

    private String state;

    private int seats;

    private int availableSeats;

    private int price;

    private Date endDate;

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Locality(String name, int seats, int price) {
        this.name = name;
        this.seats = seats;
        this.price = price;

        this.endDate = new Date(1492197025);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public int getAvailableSeats() {
        return availableSeats;
    }

    public void setAvailableSeats(int availableSeats) {
        this.availableSeats = availableSeats;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getFormatedEndDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("MMM d");
        return sdf.format(endDate);
    }
}
