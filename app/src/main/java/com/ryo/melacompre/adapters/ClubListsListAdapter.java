package com.ryo.melacompre.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.melacompre.ryo.melacompre.R;
import com.ryo.melacompre.activities.ListRegistrationActivity;
import com.ryo.melacompre.helpers.ActivityHelpers;
import com.ryo.melacompre.models.Event;
import com.ryo.melacompre.models.Locality;

import java.util.ArrayList;

/**
 * Created by ryogi on 14/04/17.
 */

public class ClubListsListAdapter extends ArrayAdapter<Event> {
    public ClubListsListAdapter(@NonNull Context context, ArrayList<Event> items) {
        super(context, R.layout.list_list_club_activity_item, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Event e = getItem(position);

        if(convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_list_club_activity_item, parent, false);
        }

        TextView tvLocalityText = (TextView) convertView.findViewById(R.id.list_list_club_item_locality_text);
        TextView tvStateText = (TextView) convertView.findViewById(R.id.list_list_club_item_state_text);
        TextView tvPriceText = (TextView) convertView.findViewById(R.id.list_list_club_item_price_text);
        tvPriceText.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getContext(), ListRegistrationActivity.class);
                getContext().startActivity(i);
            }
        });

        //Typeface font = Typeface.createFromAsset(getContext().getAssets(), "fonts/Gotham/Gotham-Light.ttf");
        tvLocalityText.setTypeface(ActivityHelpers.getTypeFace(getContext()));
        tvStateText.setTypeface(ActivityHelpers.getTypeFace(getContext()));

        tvLocalityText.setText(e.getName());
        tvStateText.setText(e.getFormatDate("MMMM d"));

        return convertView;
    }
}
