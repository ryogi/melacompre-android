package com.ryo.melacompre.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.res.ResourcesCompat;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.melacompre.ryo.melacompre.R;
import com.ryo.melacompre.helpers.ActivityHelpers;
import com.ryo.melacompre.models.Locality;

import java.util.ArrayList;

/**
 * Created by ryogi on 29/12/16.
 */

public class TopEventsListAdapter extends ArrayAdapter<Locality> {

    private int rank;

    public TopEventsListAdapter(@NonNull Context context, ArrayList<Locality> items, int rank) {
        super(context, R.layout.list_top_events_activity_item, items);
        this.rank = rank;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Locality locality = getItem(position);
        if(convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_top_events_activity_item, parent, false);
        }

        TextView tvIconText = (TextView) convertView.findViewById(R.id.list_event_item_icon_text);
        switch(rank) {
            case 0:
                tvIconText.setTextColor(ResourcesCompat.getColor(convertView.getResources(), R.color.IDgreen, null));
                break;
            case 1:
                tvIconText.setTextColor(ResourcesCompat.getColor(convertView.getResources(), R.color.IDyellow, null));
                break;
            case 2:
                tvIconText.setTextColor(ResourcesCompat.getColor(convertView.getResources(), R.color.IDpurple, null));
                break;
            default:
                tvIconText.setTextColor(ResourcesCompat.getColor(convertView.getResources(), R.color.IDblue, null));
                break;
        }

        TextView tvLocalityText = (TextView) convertView.findViewById(R.id.list_event_item_locality_text);
        TextView tvStateText = (TextView) convertView.findViewById(R.id.list_event_item_state_text);
        TextView tvPriceText = (TextView) convertView.findViewById(R.id.list_event_item_price_text);

        //Typeface font = Typeface.createFromAsset(getContext().getAssets(), "fonts/Gotham/Gotham-Light.ttf");
        tvLocalityText.setTypeface(ActivityHelpers.getTypeFace(getContext()));
        tvStateText.setTypeface(ActivityHelpers.getTypeFace(getContext()));
        tvPriceText.setTypeface(ActivityHelpers.getTypeFace(getContext()));

        tvLocalityText.setText(locality.getName());
        tvPriceText.setText("$" + String.valueOf(locality.getPrice()));
        tvStateText.setText(Html.fromHtml(locality.getState() +
                "<br /><small><font color='#646566'>Hasta " + locality.getFormatedEndDate() + "</font></small>"));

        return convertView;
    }
}
