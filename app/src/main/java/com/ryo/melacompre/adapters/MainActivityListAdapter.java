package com.ryo.melacompre.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.melacompre.ryo.melacompre.R;

import java.util.ArrayList;


/**
 * Created by ryogi on 19/12/16.
 */

public class MainActivityListAdapter extends ArrayAdapter<String> {
    public MainActivityListAdapter(@NonNull Context context, ArrayList<String> items) {
        super(context, R.layout.list_main_activity_item, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        String item = getItem(position);
        if(convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_main_activity_item, parent, false);
        }

        TextView tvText = (TextView) convertView.findViewById(R.id.main_list_text);
        TextView tvIconText = (TextView) convertView.findViewById(R.id.main_list_icon_text);

        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "fonts/Gotham/Gotham-Light.ttf");
        tvText.setTypeface(font);

        switch(item) {
            case "TopEvents":
                tvText.setText(R.string.top_events);
                tvIconText.setText("{ion-ios-star-outline}");
                break;
            case "Calendar":
                tvText.setText(R.string.calendar);
                tvIconText.setText("{ion-ios-calendar-outline}");
                break;
            case "ClubLists":
                tvText.setText(R.string.clubs_lists);
                tvIconText.setText("{ion-ios-list-outline}");
                break;
            case "Promos":
                tvText.setText(R.string.promos);
                tvIconText.setText("{ion-ios-pricetags-outline}");
                tvIconText.setTextColor(Color.parseColor("#33B5E5"));
                break;
            case "Points":
                tvText.setText(R.string.points);
                tvIconText.setText("{ion-load-a}");
                break;
            default:
                break;
        }
        return convertView;
    }
}
