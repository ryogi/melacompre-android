package com.ryo.melacompre.adapters;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.melacompre.ryo.melacompre.R;
import com.ryo.melacompre.activities.ListRegistrationActivity;
import com.ryo.melacompre.helpers.ActivityHelpers;
import com.ryo.melacompre.models.Event;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by ryogi on 7/05/17.
 */

public class CalendarActivityListAdapter extends ArrayAdapter<Event> {

    public CalendarActivityListAdapter(@NonNull Context context, ArrayList<Event> items) {
        super(context, R.layout.list_list_club_activity_item, items);
    }

    @TargetApi(Build.VERSION_CODES.M)
    public View getView(int position, View convertView, ViewGroup parent) {
        Event e = getItem(position);

        if(convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_calendar_activity_item, parent, false);
        }

        LinearLayout l = (LinearLayout)  convertView.findViewById(R.id.c_i);
        Random rand = new Random();
        l.setBackgroundColor(Color.argb(100,rand.nextInt(255),rand.nextInt(255),rand.nextInt(255)));
        TextView tvLocalityText = (TextView) convertView.findViewById(R.id.list_calendar_item_locality_text);
        TextView tvStateText = (TextView) convertView.findViewById(R.id.list_calendar_item_state_text);
        TextView tvPriceText = (TextView) convertView.findViewById(R.id.list_calendar_item_price_text);
        tvPriceText.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getContext(), ListRegistrationActivity.class);
                getContext().startActivity(i);
            }
        });

        //Typeface font = Typeface.createFromAsset(getContext().getAssets(), "fonts/Gotham/Gotham-Light.ttf");
        tvLocalityText.setTypeface(ActivityHelpers.getTypeFace(getContext()));
        tvStateText.setTypeface(ActivityHelpers.getTypeFace(getContext()));

        tvLocalityText.setText(e.getName());
        tvStateText.setText(e.getFormatDate("MMMM d"));



        return convertView;
    }
}
