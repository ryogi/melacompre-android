package com.ryo.melacompre.activities;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.melacompre.ryo.melacompre.R;
import com.mikepenz.iconics.context.IconicsContextWrapper;
import com.ryo.melacompre.adapters.ClubListsListAdapter;
import com.ryo.melacompre.helpers.ActivityHelpers;
import com.ryo.melacompre.helpers.DataModelHelper;
import com.ryo.melacompre.models.Club;

import java.util.ArrayList;

public class ListClubsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_clubs);

        ActivityHelpers.setCustomActionBar(this, R.string.club_lists_acticity_title, false);

        LinearLayout root = (LinearLayout) findViewById(R.id.list_list_club_root_layout);

        ArrayList<Club> clubs = DataModelHelper.createRandomClubs(0);

        // @todo -- Hacer el loop de los meses --
        LayoutInflater li = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        ListView lv = null;
        if(lv == null) {
            lv = (ListView) li.inflate(R.layout.list_top_events, root, false);
        }
        View header = li.inflate(R.layout.list_list_club_header, lv, false);

        ClubListsListAdapter listAdapter = new ClubListsListAdapter(this, clubs.get(0).getEvents());
        lv.setAdapter(listAdapter);

        TextView headTitle = (TextView) header.findViewById(R.id.list_list_club_header_title);
        headTitle.setTypeface(ActivityHelpers.getTypeFace(this));
        //@todo nombre del mes
        headTitle.setText("ABRIL");

        TextView headDate = (TextView) header.findViewById(R.id.list_list_club_header_date);
        headDate.setTypeface(ActivityHelpers.getTypeFace(this));
        headDate.setText("Fiestas del mes de");

        root.addView(lv);
        lv.addHeaderView(header);
        ActivityHelpers.setListViewHeightBasedOnChildren(lv);
        header.setLayoutParams(new ViewGroup.LayoutParams(ListView.LayoutParams.MATCH_PARENT,
                ListView.LayoutParams.MATCH_PARENT));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu m) {
        getMenuInflater().inflate(R.menu.main_menu, m);
        return true;
    }

    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(IconicsContextWrapper.wrap(newBase));
    }
}
