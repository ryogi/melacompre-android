package com.ryo.melacompre.activities;

import android.content.Context;
import android.os.Bundle;
import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;

import com.melacompre.ryo.melacompre.R;
import com.mikepenz.iconics.context.IconicsContextWrapper;
import com.ryo.melacompre.helpers.ActivityHelpers;

public class CheckoutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        ActivityHelpers.setCustomActionBar(this, R.string.top_events_acticity_title, false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu m) {
        getMenuInflater().inflate(R.menu.main_menu, m);
        return true;
    }

    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(IconicsContextWrapper.wrap(newBase));
    }

}
