package com.ryo.melacompre.activities;

import android.content.DialogInterface;
import android.os.Debug;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.melacompre.ryo.melacompre.R;

public class EventActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        TextView tvAddTicket = (TextView) findViewById(R.id.add_ticket);
        TextView tvSubTicket = (TextView) findViewById(R.id.sub_ticket);
        final TextView tvDeliveryPriceValue = (TextView) findViewById(R.id.delivery_price_value);
        final TextView tvNumberTickets = (TextView) findViewById(R.id.number_of_tickets);
        final TextView tvVoidPoints = (TextView) findViewById(R.id.void_points);
        final TextView tvTotalVP = (TextView) findViewById(R.id.total_buy_points);

        tvAddTicket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.w("Click", "Clicked");
                int nt = Integer.parseInt(tvNumberTickets.getText().toString());
                int vp = Integer.parseInt(tvVoidPoints.getText().toString());

                tvNumberTickets.setText(String.valueOf(++nt));

                vp += 25;
                tvVoidPoints.setText(String.valueOf(vp));
                tvTotalVP.setText("+" + vp + " Puntos Void");

                if(nt >= 2) {
                    tvDeliveryPriceValue.setText("Gratis");
                }
            }
        });

        tvSubTicket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int nt = Integer.parseInt(tvNumberTickets.getText().toString());
                Log.w("Click", "Clicked " + nt);
                if (nt != 1) {
                    tvNumberTickets.setText(String.valueOf(--nt));
                    int vp = Integer.parseInt(tvVoidPoints.getText().toString());

                    vp -= 25;
                    tvTotalVP.setText("+" + vp + " Puntos Void");
                    tvVoidPoints.setText(String.valueOf(vp));
                }

                if(nt < 2) {
                    tvDeliveryPriceValue.setText("$8500");
                }
            }
        });


        findViewById(R.id.button_buy).bringToFront();
    }
}
