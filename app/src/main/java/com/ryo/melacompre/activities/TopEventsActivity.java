package com.ryo.melacompre.activities;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.DrawableContainer;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.melacompre.ryo.melacompre.R;
import com.mikepenz.iconics.context.IconicsContextWrapper;
import com.ryo.melacompre.adapters.TopEventsListAdapter;
import com.ryo.melacompre.helpers.ActivityHelpers;
import com.ryo.melacompre.helpers.DataModelHelper;
import com.ryo.melacompre.models.Event;

import java.util.ArrayList;


/**
 * Created by ryogi on 28/12/16.
 */

public class TopEventsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_events);

        LinearLayout root = (LinearLayout) findViewById(R.id.list_top_events_root_layout);

        ActivityHelpers.setCustomActionBar(this, R.string.top_events_acticity_title, false);
        ArrayList<Event> events = DataModelHelper.createRandomEvents(3);

        int rank = 0;
        for (Event e: events) {
            LayoutInflater li = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            ListView lv = null;
            if(lv == null) {
                lv = (ListView) li.inflate(R.layout.list_top_events, root, false);
            }
            View header = li.inflate(R.layout.list_top_events_header, lv, false);

            TopEventsListAdapter listAdapter = new TopEventsListAdapter(this, e.getLocalities(), rank);
            lv.setAdapter(listAdapter);

            TextView headTitle = (TextView) header.findViewById(R.id.list_top_events_header_title);
            headTitle.setTypeface(ActivityHelpers.getTypeFace(this));
            headTitle.setText(e.getName().toUpperCase());

            switch(rank) {
                case 0:
                    header.setBackgroundResource(R.drawable.list_top_events_header);
                    headTitle.setTextColor(ResourcesCompat.getColor(getResources(), R.color.IDgreen, null));
                    break;
                case 1:
                    header.setBackgroundResource(R.drawable.list_top_events_header_y);
                    headTitle.setTextColor(ResourcesCompat.getColor(getResources(), R.color.IDyellow, null));
                    break;
                case 2:
                    header.setBackgroundResource(R.drawable.list_top_events_header_p);
                    headTitle.setTextColor(ResourcesCompat.getColor(getResources(), R.color.IDpurple, null));
                    break;
                default:
                    header.setBackgroundResource(R.drawable.list_top_events_header_b);
                    headTitle.setTextColor(ResourcesCompat.getColor(getResources(), R.color.IDblue, null));
                    break;
            }

            TextView headDate = (TextView) header.findViewById(R.id.list_top_events_header_date);
            headDate.setTypeface(ActivityHelpers.getTypeFace(this));
            headDate.setText(e.getFormatDate("EEE d MMM yyyy - HH:mm").toUpperCase());

            root.addView(lv);
            lv.addHeaderView(header);
            ActivityHelpers.setListViewHeightBasedOnChildren(lv);
            header.setLayoutParams(new ViewGroup.LayoutParams(ListView.LayoutParams.MATCH_PARENT,
                    ListView.LayoutParams.MATCH_PARENT));
            rank++;

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu m) {
        getMenuInflater().inflate(R.menu.main_menu, m);
        return true;
    }

    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(IconicsContextWrapper.wrap(newBase));
    }
}
