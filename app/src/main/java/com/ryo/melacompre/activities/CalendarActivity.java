package com.ryo.melacompre.activities;

import android.content.Context;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.melacompre.ryo.melacompre.R;
import com.mikepenz.iconics.context.IconicsContextWrapper;
import com.ryo.melacompre.adapters.CalendarActivityListAdapter;
import com.ryo.melacompre.helpers.ActivityHelpers;
import com.ryo.melacompre.helpers.DataModelHelper;
import com.ryo.melacompre.models.Event;

import java.util.ArrayList;

public class CalendarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        ActivityHelpers.setCustomActionBar(this, R.string.calendar_acticity_title, false);

        LinearLayout root = (LinearLayout) findViewById(R.id.list_calendar_root_layout);

        LayoutInflater li = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        ListView lv = null;
        if(lv == null) {
            lv = (ListView) li.inflate(R.layout.list_top_events, root, false);
        }
        View header = li.inflate(R.layout.list_calendar_header, lv, false);

        ArrayList<Event> events = DataModelHelper.createRandomEvents(5);

        CalendarActivityListAdapter listAdapter = new CalendarActivityListAdapter(this, events);
        lv.setAdapter(listAdapter);

        TextView headTitle = (TextView) header.findViewById(R.id.list_calendar_header_title);
        headTitle.setTypeface(ActivityHelpers.getTypeFace(this));
        //@todo nombre del mes
        headTitle.setText("ABRIL");

        TextView headDate = (TextView) header.findViewById(R.id.list_calendar_header_date);
        headDate.setTypeface(ActivityHelpers.getTypeFace(this));
        headDate.setText("Fiestas del mes de");

        root.addView(lv);
        lv.addHeaderView(header);
        ActivityHelpers.setListViewHeightBasedOnChildren(lv);
        header.setLayoutParams(new ViewGroup.LayoutParams(ListView.LayoutParams.MATCH_PARENT,
                ListView.LayoutParams.MATCH_PARENT));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu m) {
        getMenuInflater().inflate(R.menu.main_menu, m);
        return true;
    }

    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(IconicsContextWrapper.wrap(newBase));
    }
}
