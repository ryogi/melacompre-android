package com.ryo.melacompre.activities;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;

import com.melacompre.ryo.melacompre.R;
import com.mikepenz.iconics.context.IconicsContextWrapper;
import com.ryo.melacompre.helpers.ActivityHelpers;

public class PromoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promo);
        ActivityHelpers.setCustomActionBar(this, R.string.promo_activity_title, true);
        findViewById(R.id.on_top_layout_promo).bringToFront();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu m) {
        getMenuInflater().inflate(R.menu.main_menu, m);
        return true;
    }

    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(IconicsContextWrapper.wrap(newBase));
    }
}
