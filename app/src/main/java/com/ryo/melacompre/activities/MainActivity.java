package com.ryo.melacompre.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;

import com.melacompre.ryo.melacompre.R;
import com.mikepenz.iconics.context.IconicsContextWrapper;
import com.ryo.melacompre.adapters.MainActivityListAdapter;
import com.ryo.melacompre.helpers.ActivityHelpers;

import java.util.ArrayList;
import java.util.Arrays;


public class MainActivity extends AppCompatActivity {

    private String[] items = {
            "TopEvents",
            "Calendar",
            "ClubLists",
            "Promos",
            "Points"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        ActivityHelpers.setCustomActionBar(this, R.string.main_acticity_title, false);

        final ListView listView = (ListView) findViewById(R.id.main_list);
        final MainActivityListAdapter listAdapter =
        new MainActivityListAdapter(this, new ArrayList<>(Arrays.asList(items)));
        listView.setAdapter(listAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
                String item = listAdapter.getItem(pos);
                Intent i;
                switch(item) {
                    case "TopEvents":
                        i  = new Intent(MainActivity.this, TopEventsActivity.class);
                        startActivity(i);
                        break;
                    case "Calendar":
                        i  = new Intent(MainActivity.this, CalendarActivity.class);
                        startActivity(i);
                        /*i  = new Intent(MainActivity.this, EventActivity.class);
                        startActivity(i);*/
                        break;
                    case"ClubLists":
                        i  = new Intent(MainActivity.this, ListClubsActivity.class);
                        startActivity(i);
                        break;
                    case "Promos":
                        i  = new Intent(MainActivity.this, PromoActivity.class);
                        startActivity(i);
                        break;
                    case "Points":
                        //i  = new Intent(MainActivity.this, PointsActivity.class);
                        i  = new Intent(MainActivity.this, CheckoutActivity.class);
                        startActivity(i);
                        break;
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu m) {
        getMenuInflater().inflate(R.menu.main_menu, m);
        return true;
    }

    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(IconicsContextWrapper.wrap(newBase));
    }
}
