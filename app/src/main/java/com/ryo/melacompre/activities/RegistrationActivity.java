package com.ryo.melacompre.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;

import com.melacompre.ryo.melacompre.R;

public class RegistrationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().hide();
        setTheme(R.style.MelacompreTheme_NoActionBar);
        setContentView(R.layout.activity_registration);
    }
}
