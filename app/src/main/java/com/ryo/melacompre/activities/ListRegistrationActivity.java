package com.ryo.melacompre.activities;

import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.melacompre.ryo.melacompre.R;
import com.ryo.melacompre.helpers.ActivityHelpers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ListRegistrationActivity extends BaseActivity implements View.OnClickListener {

    HashMap<EditText, EditText> peopleData;
    TableRow trAddButton;
    TextView tvAddGuest;
    TableLayout addPeopleTable;
    ScrollView scrollList;
    TextView tvRegistrationListPoints;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_registration);
        setTitle("Club");
        findViewById(R.id.on_top_layout).bringToFront();

        peopleData = new HashMap<>();

        addPeopleTable = (TableLayout) findViewById(R.id.add_people_table_layout);

        scrollList = (ScrollView) findViewById(R.id.scroll_list_registration);

        final EditText tvGuestName = (EditText) findViewById(R.id.list_registration_name);
        final EditText tvGuestId = (EditText) findViewById(R.id.list_registration_id);
        tvAddGuest = (TextView) findViewById(R.id.tv_add_guest);
        trAddButton = (TableRow) findViewById(R.id.tr_add_button);
        tvRegistrationListPoints = (TextView) findViewById(R.id.lsit_registration_points);
        tvAddGuest.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        TableRow tr = new TableRow(addPeopleTable.getContext());
        TableRow trAddButtonNew = new TableRow(tr.getContext());

        EditText tvGuestNameNew = new EditText(addPeopleTable.getContext());
        EditText tvGuestIdNew = new EditText(addPeopleTable.getContext());
        TextView tvAddGuestNew = new TextView(addPeopleTable.getContext());
        tvAddGuestNew.setText("+");
        tvAddGuestNew.setClickable(true);
        tvAddGuestNew.setTextSize(32);
        tvAddGuestNew.setOnClickListener(this);

        addPeopleTable.removeView(trAddButton);
        trAddButton.removeAllViews();

        trAddButtonNew.addView(tvAddGuestNew);

        tr.addView(tvGuestNameNew);
        tr.addView(tvGuestIdNew);
        tr.addView(trAddButtonNew);

        //trAddButtonNew.setLayoutParams(trAddButton.getLayoutParams());
        trAddButton.removeView(tvAddGuest);
        trAddButtonNew.addView(tvAddGuest);

        addPeopleTable.addView(tr);
        tr.bringToFront();

        peopleData.put(tvGuestNameNew, tvGuestIdNew);
        trAddButton = trAddButtonNew;
        tvAddGuest = tvAddGuestNew;

        int pts = Integer.parseInt(tvRegistrationListPoints.getText().toString().split(" ")[0]) + 50;
        tvRegistrationListPoints.setText(String.valueOf(pts) + " PUNTOS");
        tvGuestNameNew.requestFocus();
    }
}
