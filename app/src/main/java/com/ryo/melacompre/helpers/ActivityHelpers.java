package com.ryo.melacompre.helpers;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.melacompre.ryo.melacompre.R;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by ryogi on 16/12/16.
 */

public class ActivityHelpers {

    public static void setCustomActionBar(final AppCompatActivity source, int id, boolean bringToFront) {

        Toolbar t = (Toolbar) source.findViewById(R.id.toolbar);
        TextView toolbarTitle = (TextView) source.findViewById(R.id.toolbar_title);

        toolbarTitle.setText(id);

        source.setSupportActionBar(t);
        source.getSupportActionBar().setDisplayShowTitleEnabled(false);
        t.inflateMenu(R.menu.main_menu);
        if(bringToFront)
            t.bringToFront();
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();

        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            if (listItem instanceof ViewGroup) {
                listItem.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));
            }

            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    public static Typeface getTypeFace(Context c) {
        Typeface font = Typeface.createFromAsset(c.getAssets(), "fonts/Gotham/Gotham-Light.ttf");
        return font;
    }

    public static Drawable getDrawableFromUrl(String url) throws Exception {
        Bitmap x;

        final HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
        connection.connect();
        InputStream input = connection.getInputStream();

        x = BitmapFactory.decodeStream(input);
        return new BitmapDrawable(x);

    }
}
