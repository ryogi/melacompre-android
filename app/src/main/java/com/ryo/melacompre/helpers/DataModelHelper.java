package com.ryo.melacompre.helpers;

import android.text.format.DateUtils;

import com.ryo.melacompre.models.Club;
import com.ryo.melacompre.models.Event;
import com.ryo.melacompre.models.Locality;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

/**
 * Created by ryogi on 29/12/16.
 */

public class DataModelHelper {

    public static ArrayList<Event> createRandomEvents(int nEvents) {
        ArrayList<Event> r = new ArrayList<>();
        for(int i = 0; i < nEvents; i++) {
            Event e = new Event("EventName " + (int) (Math.random()*100));
            e.setClub(new Club("CLubName " + (int) (Math.random()*100)));
            e.setDate(new Date());
            e.setName("Party" + (int) (Math.random()*100));

            ArrayList<Locality> l = new ArrayList<>();
            for(int j = 0; j < 3; j++){
                Locality lo = new Locality("General " + j, (int)Math.random()*10, (int)Math.random()*100);
                lo.setState("Preventa " + (int) (Math.random()*10));
                lo.setPrice((int) (Math.random()*1000));

                Random ra =new Random();
                long unixtime = (long) (1293861599+ra.nextDouble()*60*60*24*365);
                lo.setEndDate(new Date(unixtime));

                l.add(lo);
            }
            e.setLocalities(l);
            r.add(e);
        }
        return r;
    }

    public static ArrayList<Club> createRandomClubs(int nCLubs) {
        ArrayList<Club> clubs = new ArrayList<>();

        Club c = new Club("VLAK");
        Event e = new Event("Marco Bailey");
        Random ra =new Random();
        long unixtime = (long) (1293861599+ra.nextDouble()*60*60*24*365);

        e.setDate(new Date(unixtime));

        c.addEvent(e);
        clubs.add(c);

        return clubs;
    }

}
